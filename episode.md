#!/bin/bash

# Prompt user for input
read -p "Enter the year (YYYY): " year
read -p "Enter the month (MM): " month
read -p "Enter the day (DD): " day
read -p "Enter the episode number: " episode
read -p "Enter the season number: " season
read -p "Enter the title: " title
read -p "Enter the YouTube URL: " youtube_url
read -p "Enter the Fireside URL: " fireside_url
read -p "Enter the description: " description

# Construct the date
date="${year}-${month}-${day}"

# Construct the path to the additional markdown file
file_path="/home/matt/mhome/tlc-files/Podcast Files/Notes/Season ${season}/${season}${episode}/${season}${episode}.md"

# Check if the file exists
if [ ! -f "$file_path" ]; then
    echo "Error: File $file_path does not exist. Exiting."
    exit 1
fi

# Read the additional content
additional_content=$(cat "$file_path")

# Define the output filename
filename="${episode}.md"

# Construct the markdown content
cat <<EOF > "$filename"
---
title: $title
description: $description
eleventyExcludeFromCollections: true
date: $date
tags: podcast
layout: layouts/post.njk
build:
  list: never
---

Season $season, Episode $episode

{{< youtube "$youtube_url" >}}

{{< fireside "$fireside_url" >}}

---

$additional_content
EOF

echo "Markdown file generated as '$filename'."

