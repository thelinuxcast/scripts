#!/bin/bash

theme="$HOME/mhome/Downloads/gitthings/archie"
site="$HOME/mhome/Documents/Pages/hugo/TLC"

cd "$theme"
git status
read -p "Git Commit Message: " COMMIT
git add .
git commit -m "$COMMIT"
git push

cd "$site"
git submodule update --force --recursive --init --remote
git status
read -p "Git Commit Message: " COMMIT
git add .
git commit -m "$COMMIT"
git push
read -p "DONE! Press Enter"

