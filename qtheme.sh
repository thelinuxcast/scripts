#!/bin/bash


#Import some variables

qconf="$HOME/myrepo/qtile/config.py"
def="$HOME/myrepo/qtile/qtiledefault.py"
themes="$HOME/myrepo/qtile/themes/"
alconf="$HOME/myrepo/alacritty/"
rofi="$HOME/myrepo/rofi/"
walls="$HOME/mhome/Pictures/walls/otherwalls/ricewalls"
dunst="$HOME/myrepo/dunst/dunstrc.d/"
picom="$HOME/myrepo/picom/picom.conf"

# Out put theme options
declare -a options=(
"pywal"
#"transparent"
"text"
"dwm"
"catppuccin"
"cozytile"
"colorful"
"onedark"
"dracula"
"everforest"
"gruvbox"
"moonfly"
"nord"
"monokai"
"simple"
"retro"
"whitey"
"fraggle"
"smallbar"
"gruvbox_shapes"
"gruvbox_light"
"everforest_shapes"
"everforest_slants"
"dracula_shapes"
"black"
"gruvbox_powerline"
"gruvbox_chadwm"
"catppuccin_chadwm"
"everforest_chadwm"
"bottom"
"quit"
)

# Display theme options in rofi and save as CHOICE.
choice=$(printf '%s\n' "${options[@]}" | rofi -dmenu -i -l 20 -p 'Themes')

# Enable quit option
if [ $choice = 'quit' ]; then
    echo "No theme selected"
    exit
fi

# Copy the choice to the working config.py

cp $themes/$choice.py $qconf

# Copy qtile theme specific config file to config.py, enable rofi theme for CHOICE, do same with Dunst.
#sed -i -E "s%(colors, backgroundColor, foregroundColor, workspaceColor, chordColor = colors\.)\w+\(\)%\1${choice}()%" $qconf
cp $rofi/$choice.rasi $rofi/theme.rasi
cp $dunst/01-$choice.conf $dunst/99-theme.conf


# Set Kitty Theme

case $choice in
    catppuccin)
        kitty +kitten themes --reload-in=all Catppuccin-Mocha
        ;;
    onedark)
        kitty +kitten themes --reload-in=all One Dark
        ;;
    monokai)
        kitty +kitten themes --reload-in=all Monokai Pro
        ;;
    dracula)
        kitty +kitten themes --reload-in=all Dracula
        ;;
    dracula_shapes)
        kitty +kitten themes --reload-in=all Dracula
        ;;
    everforest_slants)
        kitty +kitten themes --reload-in=all Everforest Dark Hard
        ;;
    everforest)
        kitty +kitten themes --reload-in=all Everforest Dark Hard
        ;;
    everforest_chadwm)
        kitty +kitten themes --reload-in=all Everforest Dark Hard
        ;;
    fraggle)
        kitty +kitten themes --reload-in=all Everforest Dark Hard
        ;;
    gruvbox_light)
        kitty +kitten themes --reload-in=all Gruvbox Light Hard
        ;;
    gruvbox)
        kitty +kitten themes --reload-in=all Gruvbox Dark Hard
        ;;
    dwm)
        kitty +kitten themes --reload-in=all Gruvbox Dark Hard
        ;;
    gruvbox_powerline)
        kitty +kitten themes --reload-in=all Gruvbox Dark Hard
        ;;
    colorful)
        kitty +kitten themes --reload-in=all Gruvbox Dark Hard
        ;;
    moonfly)
        kitty +kitten themes --reload-in=all Tomorrow Night
        ;;
    simple)
        kitty +kitten themes --reload-in=all Nord
        ;;
    nord)
        kitty +kitten themes --reload-in=all Nord
        ;;
    retro)
        kitty +kitten themes --reload-in=all IC Orange PPL
        ;;
    whitey)
        kitty +kitten themes --reload-in=all Aquarium Light
        ;;
    smallbar)
        kitty +kitten themes --reload-in=all Afterglow
        ;;
    gruvbox_shapes)
        kitty +kitten themes --reload-in=all Gruvbox Dark Hard
        ;;
    everforest_shapes)
        kitty +kitten themes --reload-in=all Everforest Dark Hard
        ;;
    black)
        kitty +kitten themes --reload-in=all black
        ;;
    catppuccin_chadwm)
        kitty +kitten themes --reload-in=all Catppuccin-Mocha
        ;;
    gruvbox_chadwm)
        kitty +kitten themes --reload-in=all Gruvbox Dark Hard
        ;;
esac

# Restart Qtile

qtile cmd-obj -o cmd -f restart 

# Change Wallpaper
#
feh --bg-fil $walls/$choice.jpg

