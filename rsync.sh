#!/bin/bash
date=$(date +%Y.%m.%d-%H.%M.%S)
excfile="$HOME/media/Documents/exclude.txt"
#txtfile="/hub/Backups/2023/opensuse/mainbackup/backuplist.txt"

#echo $date >> $txtfile 

rsync -rv /home/matt/* --exclude 'smssd/' --exclude 'media/Music' --exclude 'hub/' --exclude 'mhome/Downloads/isos' --exclude '.local/share/' --exclude '.cache' --exclude 'mhome/Downloads/gitthings' matt@192.168.86.120:/homelab/Backups/2023/opensuse/newbackup

notify-send -h string:fgcolor:#ff4444 "back up running"
