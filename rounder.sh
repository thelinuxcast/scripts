picom="$HOME/.config/picom/picom.conf"

 # Change the values to 15
sed -i 's/corner-radius\s*=\s*'"$corner_radius"'/corner-radius = 15/' "$picom"
sed -i 's/rounded-borders\s*=\s*'"$rounded_borders"'/rounded-borders=15/' "$picom"

    # Check if the values are not already 0, and set them to 0 if necessary
#    if [ "$corner_radius" != "0" ] || [ "$rounded_borders" != "0" ]; then
#        sed -i 's/corner-radius\s*=\s*'"$corner_radius"'/corner-radius = 0/' "$picom"
#        sed -i 's/rounded-borders\s*=\s*'"$rounded_borders"'/rounded-borders=0/' "$picom"
#    fi
