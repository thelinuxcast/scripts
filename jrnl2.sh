#!/bin/bash

day=$(date +"%d")
month=$(date +"%b")
year=$(date +"%Y")

#CD to Journal Directory
file=/home/matt/mhome/nextcloud/Journal

#Check to see if Year folder exists
if [ -d ""$file"/"$year"/" ]; then
    cd $file/$year
else
    mkdir $file/$year && cd $file/$year
fi

#Check to see if Month folder exists
if [ -d ""$file"/"$year"/"$month"" ]; then
    cd $file/$year/$month
else
    mkdir $file/$year/$month && cd $file/$year/$month
fi

#create jnrl entry for date
touch "$day"-"$month".md
nvim "$day"-"$month".md
