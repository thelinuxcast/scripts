#!/bin/bash
# Author: Matthew Weber AKA The Linux Cast
# Version: 0.1 
# Description: This script allows you to change between several Hyperland/Waybar Rices.
# Dependencies: hyprland, waybar
##########################################################################

# Variables
# If you use this script, ensure your paths are right, they may be different than mine.
#

hyprland="$HOME/.config/hypr/modules/general.conf"
hyptheme="$HOME/.config/hypr/themes"
waybar="$HOME/.config/waybar/config.jsonc"
style="$HOME/.config/waybar/style.css"
waytheme="$HOME/.config/waybar/themes"
walls="$HOME/mhome/Pictures/walls/otherwalls/ricewalls"

# Out put theme options
declare -a options=(
"catppuccin"
"ayu"
"dracula"
"gruvbox"
"evergarden"
"gruvbox"
"quit"
)

# Display theme options in rofi and save as CHOICE.
choice=$(printf '%s\n' "${options[@]}" | rofi -dmenu -i -l 20 -p 'Themes')

# Enable quit option
if [ $choice = 'quit' ]; then
    echo "No theme selected"
    exit
fi

#Copy Config Files to the Appropriate Places. Placeholder files must exist.
cp $hyptheme/$choice.conf $hyprland
cp $waytheme/$choice.jsonc $waybar
cp $waytheme/$choice.css $style
cp $rofi/$choice.rasi $rofi/theme.rasi
cp $dunst/01-$choice.conf $dunst/99-theme.conf


# Kill & Restart Waybar

killall waybar
waybar &

# Set Kitty Themes
#

case $choice in
    catppuccin)
        kitty +kitten themes --reload-in=all Catppuccin-Mocha
        ;;
    kanagawa)
        kitty +kitten themes --reload-in=all Kanagawa
        ;;
    onedark)
        kitty +kitten themes --reload-in=all One Dark
        ;;
    sonokai)
        kitty +kitten themes --reload-in=all Sonokai
        ;;
    dracula)
        kitty +kitten themes --reload-in=all Dracula
        ;;
    dracula_shapes)
        kitty +kitten themes --reload-in=all Dracula
        ;;
    adaptive)
        kitty +kitten themes --reload-in=all Everforest Dark Hard
        ;;
    everforest)
        kitty +kitten themes --reload-in=all Everforest Dark Hard
        ;;
    evergarden)
        kitty +kitten themes --reload-in=all Everforest Dark Hard
        ;;
    ocean)
        kitty +kitten themes --reload-in=all Oceanic Material
        ;;
    dwm_gruvbox)
        kitty +kitten themes --reload-in=all Gruvbox Dark Hard
        ;;
    gruvbox)
        kitty +kitten themes --reload-in=all Gruvbox Dark Hard
        ;;
    gruvbox_powerline)
        kitty +kitten themes --reload-in=all Gruvbox Dark Hard
        ;;
    moonfly)
        kitty +kitten themes --reload-in=all Tomorrow Night
        ;;
    nord)
        kitty +kitten themes --reload-in=all nord
        ;;
    tokyonight)
        kitty +kitten themes --reload-in=all Tokyo Night
        ;;
    tomorrow-night)
        kitty +kitten themes --reload-in=all Tomorrow Night
        ;;
    dwm)
        kitty +kitten themes --reload-in=all Everforest Dark Hard
        ;;
    beach)
        kitty +kitten themes --reload-in=all Dayfox
        ;;
    ayu)
        kitty +kitten themes --reload-in=all Ayu
        ;;
esac

# Set Wallpapers
#feh --bg-fil $walls/$choice.jpg
swww img $walls/$choice.jpg


# Change vim color scheme (coming soon)

#Dunst (Disable if you do not use dunst)
killall dunst
while pgrep -u $UID -x dunst >/dev/null; do sleep 1; done
