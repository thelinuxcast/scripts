#!/bin/bash

# Original script idea using fzf from Jake@Linux. Adapted by The Linux Cast for movie watching reasons.
#
# Dependencies: fzf 

# This is the path to your movies folder
movies="/hub/Stuff/Media/Movies"

menu=$(ls -a "$movies" | uniq -u | fzf --height=100% --reverse --header-first)

exec nohup mpv "$movies"/"$menu">/dev/null 2>&1 

