# i3 Ricer Script

This is a script to change polybar, i3 and alacritty themes via rofi.

## Dependencies

* Rofi
* Polybar
* i3-gaps v4.20+
* Dunst
* Alacritty
* Nerd Fonts
* My i3, rofi, dunst, alacritty, and polybar configs, available in my dotfile repository.

## Things to Know

First, inside the script are a series of variables that lead to paths. Make sure, if you're going to use this, that you've changed the paths so they are right for your system.

Second, I change the themes a lot, so make sure if you don't want things to break, that you don't pull from this without foreknowledge that things might be different.


### Todo

- [ ] Better Documentation
- [x] Add the ability to change rofi themes
- [ ] Add the ability to change vim themes
- [ ] Add the ability to change Firefox themes (likely unrealistic)
- [ ] Add the ability to change GTK/QT themes
- [ ] Add ability to add rounded corners to certain themes
- [ ] Continue adding themes from archcraft Linux and other distros.
- [ ] Make compatible with bspwm and qtile
