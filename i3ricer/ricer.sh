#!/bin/bash

# Rofi Based Ricing script for i3 Window Manager
# Developed by Matthew Weber AKA The Linux Cast
# Version 0.3
# Last Updated 07 Aug 2022

#Variables & Paths
#IMPORTANT! Change these paths to the appropriate paths on your system. They won't be the same as mine. !IMPORTANT
i3p="$HOME/myrepo/i3/themes"
i3t="$HOME/myrepo/i3/theme.conf"
ptconf="$HOME/myrepo/polybar/themes/configs"
pconf="$HOME/myrepo/polybar/config.ini"
ptcolor="$HOME/myrepo/polybar/themes/colors"
pcolor="$HOME/myrepo/polybar/themes/theme.ini"
mods="$HOME/myrepo/polybar/themes/modules"
pmods="$HOME/myrepo/polybar/themes/modules/modules.ini"
alconf="$HOME/myrepo/alacritty/"
rofi="$HOME/myrepo/rofi/"
walls="$HOME/mhome/Pictures/walls/otherwalls/ricewalls"
piconf="$HOME/myrepo/picom/picom.conf"
dunst="$HOME/myrepo/dunst/dunstrc.d/"

declare -a options=(
"polyriver"
"kanagawa"
"cyberpunk"
"dracula"
"catppuccin"
"snowfall"
"everforest"
"pink"
"gruvbox"
"xfce_gruv"
"gruvbox_powerline"
"nord"
"ocean"
"onedark"
#"papercolor" WIP
#"papercolordark" WIP
"tokyonight"
"moonfly"
"sonokai"
"tomorrow-night"
"map"
"adaptive"
"dwm"
"dwm_gruvbox"
"bouquet"
"beach"
"keyboards"
"kiss"
"landscape"
"slime"
"manhattan"
"arrows"
"nxc"
"solarized"
"quit"
)

choice=$(printf '%s\n' "${options[@]}" | rofi -dmenu -i -l 20 -p 'Themes')

if [ $choice = 'quit' ]; then
    echo "No theme selected"
    exit
fi

#Copy Config Files to the Appropriate Places. Placeholder files must exist.
cp $i3p/$choice.conf $i3t 
cp $ptconf/$choice $pconf 
cp $mods/$choice.ini $pmods 
cp $ptcolor/$choice.ini $pcolor 
cp $rofi/$choice.rasi $rofi/theme.rasi
cp $dunst/01-$choice.conf $dunst/99-theme.conf

i3-msg restart

# Set Kitty Themes
#
#
#

case $choice in
    catppuccin)
        kitty +kitten themes --reload-in=all Catppuccin-Mocha
        ;;
    kanagawa)
        kitty +kitten themes --reload-in=all Kanagawa
        ;;
    onedark)
        kitty +kitten themes --reload-in=all One Dark
        ;;
    snowfall)
        kitty +kitten themes --reload-in=all Eldritch
        ;;
    sonokai)
        kitty +kitten themes --reload-in=all Sonokai
        ;;
    dracula)
        kitty +kitten themes --reload-in=all Dracula
        ;;
    dracula_shapes)
        kitty +kitten themes --reload-in=all Dracula
        ;;
    adaptive)
        kitty +kitten themes --reload-in=all Everforest Dark Hard
        ;;
    everforest)
        kitty +kitten themes --reload-in=all Everforest Dark Hard
        ;;
    ocean)
        kitty +kitten themes --reload-in=all Oceanic Material
        ;;
    dwm_gruvbox)
        kitty +kitten themes --reload-in=all Gruvbox Dark Hard
        ;;
    gruvbox)
        kitty +kitten themes --reload-in=all Gruvbox Dark Hard
        ;;
    gruvbox_powerline)
        kitty +kitten themes --reload-in=all Gruvbox Dark Hard
        ;;
    moonfly)
        kitty +kitten themes --reload-in=all Tomorrow Night
        ;;
    nord)
        kitty +kitten themes --reload-in=all nord
        ;;
    tokyonight)
        kitty +kitten themes --reload-in=all Tokyo Night
        ;;
    tomorrow-night)
        kitty +kitten themes --reload-in=all Tomorrow Night
        ;;
    map)
        kitty +kitten themes --reload-in=all 3024 Night
        ;;
    dwm)
        kitty +kitten themes --reload-in=all Everforest Dark Hard
        ;;
    bouquet)
        kitty +kitten themes --reload-in=all Dayfox
        ;;
    beach)
        kitty +kitten themes --reload-in=all Dayfox
        ;;
    keyboards)
        kitty +kitten themes --reload-in=all Adwaita Dark
        ;;
    kiss)
        kitty +kitten themes --reload-in=all Adwaita Dark
        ;;
    landscape)
        kitty +kitten themes --reload-in=all Blazer
        ;;
    slime)
        kitty +kitten themes --reload-in=all Dayfox
        ;;
    manhatten)
        kitty +kitten themes --reload-in=all Blazer
        ;;
    nxc)
        kitty +kitten themes --reload-in=all Adwaita Dark
        ;;
    solarized)
        kitty +kitten themes --reload-in=all Solarized Dark
        ;;
    polyriver)
        kitty +kitten themes --reload-in=all Solarized Dark
        ;;
    cyberpunk)
        kitty +kitten themes --reload-in=all Idle Toes
        ;;
esac

# Toggles Gaps off for gruvbox powerline & gruvbox & Nord
if [ $choice = 'gruvbox_powerline' ] || [ $choice = 'dwm_gruvbox' ] || [ $choice = 'gruvbox' ] || [ $choice = 'nord' ] || [ $choice = 'tomorrow-night' ]; then
    i3-msg gaps inner all set 1, gaps outer all set 1
elif [ $choice = 'adaptive' ]; then
    i3-msg gaps inner all set 5, gaps outer all set 5
fi

if [ $choice = 'xfce_gruv' ]; then
    polybar-msg cmd quit
    exec xfce4-panel --disable-wm-check &
else
    killall xfce4-panel
fi


# Set Wallpapers
feh --bg-fil $walls/$choice.jpg

# Change vim color scheme (coming soon)

#Dunst (Disable if you do not use dunst)
killall dunst
while pgrep -u $UID -x dunst >/dev/null; do sleep 1; done
