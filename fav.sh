#!/bin/bash
# A script to save the current wallpaper as a favorite.
# Written by The Linux Cast AKA Matt Weber
# Created July 12 2023
# Last Updated July 12 2023
# Version 1.

favdir="$HOME/Pictures/walls/Favorites"
filename="$HOME/.fehbg"  # Replace with the actual file name
path=$(grep -Po "(?<=--bg-fill ')[^']*" "$filename")
if [[ -n $path ]]; then
    cp $path $favdir
else
  echo "Path not found."
fi


