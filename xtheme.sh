#!/bin/bash


#Import some variables
xconf="$HOME/.config/xmonad/xmonad.hs"
rofi="$HOME/myrepo/rofi/"
walls="$HOME/Pictures/walls/otherwalls/ricewalls"
dunst="$HOME/myrepo/dunst/dunstrc.d/"
picom="$HOME/myrepo/picom/picom.conf"

# Out put theme options
declare -a options=(
#"catppuccin"
#"colorful"
"Kanagawa"
"KanagawaSquare"
"DoomOne"
"Dracula"
"Everforest"
"GruvboxDark"
"GruvboxDarkShapes"
#"gruvbox_powerline"
"MoonLight"
"Nord"
"Monokai"
"MonokaiPro"
"Himan"
"Fraggle"
"AyuMirage"
"IrBlack"
"SolarizedDark"
"TokyoNight"
"quit"
)

# Display theme options in rofi and save as CHOICE.
choice=$(printf '%s\n' "${options[@]}" | rofi -dmenu -i -l 20 -p 'Themes')

# Enable quit option
if [ $choice = 'quit' ]; then
    echo "No theme selected"
    exit
fi


sed -i "s/Colors\.[^.]\+/Colors.$choice/"  $xconf
# Copy qtile theme specific config file to config.py, enable rofi theme for CHOICE, do same with Dunst.
#cp $rofi/$choice.rasi $rofi/theme.rasi
#cp $dunst/01-$choice.conf $dunst/99-theme.conf
#cp $alconf/colorschemes/$choice.yml $alconf/colors.yml


# Set Kitty Theme

case $choice in
#    catppuccin)
#        kitty +kitten themes --reload-in=all Catppuccin-Mocha
#        ;;
    DoomOne)
        kitty +kitten themes --reload-in=all One Dark
        ;;
    Himan)
        kitty +kitten themes --reload-in=all himan
        ;;
    Monokai)
        kitty +kitten themes --reload-in=all monokai pro
        ;;
    MonokaiPro)
        kitty +kitten themes --reload-in=all monokai pro
        ;;
    Dracula)
        kitty +kitten themes --reload-in=all Dracula
        ;;
    Everforest)
        kitty +kitten themes --reload-in=all Everforest Dark Hard
        ;;
    Fraggle)
        kitty +kitten themes --reload-in=all Fraggle
        ;;
    GruvboxDark)
        kitty +kitten themes --reload-in=all Gruvbox Dark Hard
        ;;
    MoonLight)
        kitty +kitten themes --reload-in=all moonlight
        ;;
    Nord)
        kitty +kitten themes --reload-in=all Nord
        ;;
    TokyoNight)
        kitty +kitten themes --reload-in=all Tokyo Night
        ;;
    IrBlack)
        kitty +kitten themes --reload-in=all Tomorrow Night
        ;;
    SolarizedDark)
        kitty +kitten themes --reload-in=all Solarized Dark
        ;;
    AyuMirage)
        kitty +kitten themes --reload-in=all Ayu Mirage
        ;;

esac

# Restart Xmonad

xmonad --recompile; xmonad --restart

# Change Wallpaper
#
feh --bg-fil $walls/$choice.jpg

