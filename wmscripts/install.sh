#!/bin/sh

# This script will install several window managers onto your system using Matt's (AKA The Linux Cast's) dot files. At the moment, this script will install i3, dwm, hyprland, sway, and bspwm.
#
# Currently this script supports only openSUSE but will be written with the goal of supporting all distros.
#
#


# Determine distro
#

echo "Please enter the number of the distro you are using"
echo "1) openSUSE"
echo "2) Debian/Ubuntu"
echo "3) Fedora"
echo "4) Arch Linux"

read -p "Enter your choice [1-4]: " choice

case $choice in
    1)
        distro="zypper"
        ;;
    2)
        distro="apt"
        ;;
    3)
        distro="dnf"
        ;;
    4)
        distro="pacman"
        ;;
    *)
        echo "Invalid choice"
        exit 1
        ;;
esac

echo "you chose $choice which uses $distro"

# Find out what the user wants to do.
echo "What Window Manager Do You Want to Install?"
echo "1) i3wm"
echo "2) qtile"
echo "3) dwm"
echo "4) bspwm"
echo "5) hyprland"
echo "6) sway"

#
#
#
#
# Ask what window managers the user wants to install
#
# Ask if they want vanilla or riced versions
#
# list optional dependencies i.e. picom, choice of terminal, and file manager
#
#
# If user chooses riced versions of WMs then download dotfiles
# sym link dotfiles for WMs into .config
# install window managers
#
#
#
