#!/bin/sh

# This script is a component of my Window Manager install script it's purpose is to determine which distro the user is using.
#




echo "Please enter the number of the distro you are using"
echo "1) openSUSE"
echo "2) Debian/Ubuntu"
echo "3) Fedora"
echo "4) Arch Linux"

read -p "Enter your choice [1-4]: " choice

case $choice in
    1)
        selected="openSUSE"
        ;;
    2)
        selcted="Debian/Ubuntu"
        ;;
    3)
        selected="Fedora"
        ;;
    4)
        selected="Arch"
        ;;
    *)
        echo "Invalid choice"
        exit 1
        ;;
esac

echo $selected
