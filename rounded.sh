#!/bin/bash

# Path to picom config file
PICOM_CONFIG="$HOME/.config/picom/picom.conf"

# Default rounded corner radius (adjust as needed)
ROUNDED_RADIUS=10

# Check current corner-radius value
CURRENT_RADIUS=$(grep -Po '(?<=corner-radius = )\d+' "$PICOM_CONFIG")

if [[ -z "$CURRENT_RADIUS" ]]; then
    echo "Error: Could not find 'corner-radius' setting in $PICOM_CONFIG"
    exit 1
fi

if [[ "$CURRENT_RADIUS" -eq 0 ]]; then
    # Enable rounded corners
    sed -i "s/corner-radius = 0/corner-radius = $ROUNDED_RADIUS/" "$PICOM_CONFIG"
    echo "Enabled rounded corners (radius: $ROUNDED_RADIUS)."
else
    # Disable rounded corners (set to 0)
    sed -i "s/corner-radius = $CURRENT_RADIUS/corner-radius = 0/" "$PICOM_CONFIG"
    echo "Disabled rounded corners."
fi

# Restart picom to apply changes
pkill picom
picom --vsync -b &

exit 0

