#!/bin/sh

hub="/dev/sdb1"
media="/dev/sdc1"
stuff="/dev/sdd1"
backups="/dev/sde1"


#hub
if mount | grep "$hub" > /dev/null; then 
    echo "HUB Mounted"
else
    sudo mount $hub /hub
fi

#media
if mount | grep "$media" > /dev/null; then 
    echo "Media Mounted"
else
    sudo mount $media /mnt/Media
fi


#stuff
if mount | grep "$stuff" > /dev/null; then 
    echo "stuff Mounted"
else
    sudo mount $stuff /mnt/stuff
fi


#stuff
if mount | grep "$backups" > /dev/null; then 
    echo "backups Mounted"
else
    sudo mount $backups /backups
fi


