#!/bin/sh

updates=$(zypper lu | grep 'v |' | wc -l)

if [ "$updates" -gt 0 ]; then
    echo "# $updates"
else
    echo "0"
fi
