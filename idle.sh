#!/bin/bash

# Check if idle is currently inhibited
if hyprctl keyword idle | grep -q "Inhibited"; then
    # If inhibited, uninhibit the idle
    echo "Idle is currently inhibited. Uninhibiting..."
    hyprctl keyword idle uninhibit
    notify-send "Screen Lock Off"
else
    # If not inhibited, inhibit the idle
    echo "Idle is not inhibited. Inhibiting..."
    hyprctl keyword idle inhibit
    notify-send "Screen Lock On"
fi

