#!/bin/sh
choice=$(sxiv -rto $HOME/Pictures/walls/otherwalls)
echo $choice
if [[ ! $choice ]]; then
	echo "Choose an image to set as the wallpaper"; exit 1
else
	echo "setting wallpaper"
	feh --bg-scale $choice
fi

