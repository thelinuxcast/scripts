#!/bin/sh

is_process_running() {
  pgrep -x "$1" >/dev/null
}

if is_process_running "dwm"; then
  window_manager="dwm"
elif is_process_running "awesome"; then
  window_manager="awesome"
else
  window_manager="unknown"
fi

set -e
WALLPAPER_PATH="$HOME/Pictures/walls/otherwalls/illustrated/girls/"
DMENU="$(ls $WALLPAPER_PATH | dmenu -p "Set Wallpaper:" -l 20)"
SELECTED="$WALLPAPER_PATH$DMENU"
if [ -z "$DMENU"]; then
	exit 0
else
    wal -i $SELECTED
    sed -i ~/.Xresources -re '1,1000d'
    cat ~/.cache/wal/colors.Xresources >> ~/.Xresources
    pywalfox update
    killall dunst && dunst &

    pkill sxiv
    notify-send "Enjoy your new colorscheme!"
fi

