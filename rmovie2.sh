#!/bin/bash

# Directory containing movies
movie_dir="/hub/Stuff/Media/Movies/"

# Function to fetch movie art from IMDb
fetch_movie_art() {
    local movie_name="$1"
    local imdb_url="https://www.imdb.com"
    local search_url="${imdb_url}/find?s=tt&ttype=ft&q="
    
    # Search for the movie on IMDb
    local search_results=$(curl -s "${search_url}${movie_name}" | grep -m 1 -oP 'href="\K\/title\/\K[^"]+')

    if [[ -n $search_results ]]; then
        local movie_id="${search_results%%/}"

        # Get the IMDb page for the movie
        local movie_page=$(curl -s "${imdb_url}/title/${movie_id}")

        # Extract the movie poster URL
        local poster_url=$(echo "$movie_page" | grep -oP 'meta\s+property="og:image"\s+content="\K[^"]+')

        if [[ -n $poster_url ]]; then
            # Download the movie poster
            local poster_path="/tmp/${movie_id}.jpg"
            curl -s "$poster_url" -o "$poster_path"
            echo "$poster_path"
        fi
    fi
}

# Array to store movie names
movies=()

# Read movie names from directory
while IFS= read -r -d '' file; do
    movies+=("${file##*/}")
done < <(find "$movie_dir" -type f -name '*.mp4' -print0)

# Array to store poster paths
posters=()

# Fetch movie posters from IMDb
for movie in "${movies[@]}"; do
    poster_path=$(fetch_movie_art "$movie")
    if [[ -n $poster_path ]]; then
        posters+=("$poster_path")
    fi
done

# Combine movie names and poster paths
combined=()
for i in "${!movies[@]}"; do
    combined+=("${movies[$i]}|${posters[$i]}")
done

# Select a movie using Rofi
selected=$(printf '%s\n' "${combined[@]}" | rofi -dmenu -p "Select a movie:" -format d -i)

if [[ -n $selected ]]; then
    # Extract the movie name and poster path
    IFS='|' read -r -a parts <<< "$selected"
    movie="${parts[0]}"
    poster="${parts[1]}"

    # Display the movie poster using feh (optional)
    feh "$poster" &

    # Play the movie using mpv
    mpv "${movie_dir}/${movie}"
fi

